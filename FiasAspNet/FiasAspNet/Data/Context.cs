﻿using FiasAspNet.Data.RecordMaps;
using System.Data.Entity;

namespace FiasAspNet.Data
{
    public class Context : DbContext
    {
        public Context(string connectionString) : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new RegionsRecordMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}