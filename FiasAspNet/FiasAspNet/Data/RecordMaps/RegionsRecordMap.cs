﻿using FiasAspNet.Domains;
using System.Data.Entity.ModelConfiguration;

namespace FiasAspNet.Data.RecordMaps
{
    public class RegionsRecordMap : EntityTypeConfiguration<Regions>
    {
        public RegionsRecordMap()
        {
            ToTable(nameof(Regions));
            HasKey(m => new { m.Id });

            Property(region => region.ReginName);
            Property(region => region.RegionCode);
            Property(region => region.Link);
            Property(region => region.IsSelected);
        }
    }
}