﻿using FiasAspNet.Domains;
using System.Data.Entity.ModelConfiguration;

namespace FiasAspNet.Data.RecordMaps
{
    public class FiasDataRecordMap : EntityTypeConfiguration<FiasData>
    {
        public FiasDataRecordMap()
        {
            ToTable(nameof(FiasData));
            HasKey(m => new { m.Id });
            HasIndex(m => new { m.AOGuid, m.ParentGuid }).IsClustered();
        }
    }
}