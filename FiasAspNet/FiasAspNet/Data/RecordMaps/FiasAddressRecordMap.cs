﻿using FiasAspNet.Domains;
using System.Data.Entity.ModelConfiguration;

namespace FiasAspNet.Data.RecordMaps
{
    public class FiasAddressRecordMap : EntityTypeConfiguration<FiasAddress>
    {
        public FiasAddressRecordMap()
        {
            ToTable(nameof(FiasAddress));
            HasKey(m => new { m.Id });
            HasIndex(m => m.FullAddress).IsClustered();
        }
    }
}