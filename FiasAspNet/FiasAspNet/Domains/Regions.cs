﻿namespace FiasAspNet.Domains
{
    public class Regions : BaseEntity
    {
        public string ReginName { get; set; }

        public int RegionCode { get; set; }

        public bool IsSelected { get; set; }

        public string Link { get; set; }
    }
}