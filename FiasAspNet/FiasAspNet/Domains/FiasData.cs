﻿namespace FiasAspNet.Domains
{
    public class FiasData : BaseEntity
    {
        public string AOGuid { get; set; }

        public string AOLevel { get; set; }

        public string AOId { get; set; }

        public string FormalName { get; set; }

        public string ParentGuid { get; set; }

        public string RegionCode { get; set; }

        public string ShortName { get; set; }
    }
}