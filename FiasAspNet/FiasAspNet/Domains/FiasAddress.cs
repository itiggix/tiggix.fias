﻿namespace FiasAspNet.Domains
{
    public class FiasAddress : BaseEntity
    {
        public string Region { get; set; }

        public string AO { get; set; }

        public string Area { get; set; }

        public string City { get; set; }

        public string IntracityTerritory { get; set; }

        public string Locality { get; set; }

        public string Street { get; set; }

        public string FullAddress { get; set; }
    }
}