﻿namespace FiasAspNet.Domains
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}