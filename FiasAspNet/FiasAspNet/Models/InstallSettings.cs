﻿namespace FiasAspNet.Models
{
    public class InstallSettings
    {
        public string DatabaseName { get; set; }

        public string ServerName { get; set; }

        public string UserName { get; set; }

        public string UserPassword { get; set; }
    }
}